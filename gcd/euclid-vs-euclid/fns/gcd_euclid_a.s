  .text
  .globl gcd_euclid_a
  .type gcd_euclid_a, @function

#! file-offset 0x650
#! rip-offset  0x400650
#! capacity    115 bytes

# Text                            #  Line  RIP       Bytes  Opcode              
.gcd_euclid_a:                    #        0x400650  0      OPC=<label>         
  pushq %rbp                      #  1     0x400650  1      OPC=pushq_r64_1     
  movq %rsp, %rbp                 #  2     0x400651  3      OPC=movq_r64_r64    
  movq %rdi, -0x18(%rbp)          #  3     0x400654  4      OPC=movq_m64_r64    
  movq %rsi, -0x20(%rbp)          #  4     0x400658  4      OPC=movq_m64_r64    
  movq -0x18(%rbp), %rax          #  5     0x40065c  4      OPC=movq_r64_m64    
  cmpq -0x20(%rbp), %rax          #  6     0x400660  4      OPC=cmpq_r64_m64    
  jge .L_40067e                   #  7     0x400664  2      OPC=jge_label       
  movq -0x18(%rbp), %rax          #  8     0x400666  4      OPC=movq_r64_m64    
  movq %rax, -0x8(%rbp)           #  9     0x40066a  4      OPC=movq_m64_r64    
  movq -0x20(%rbp), %rax          #  10    0x40066e  4      OPC=movq_r64_m64    
  movq %rax, -0x18(%rbp)          #  11    0x400672  4      OPC=movq_m64_r64    
  movq -0x8(%rbp), %rax           #  12    0x400676  4      OPC=movq_r64_m64    
  movq %rax, -0x20(%rbp)          #  13    0x40067a  4      OPC=movq_m64_r64    
.L_40067e:                        #        0x40067e  0      OPC=<label>         
  movq -0x18(%rbp), %rax          #  14    0x40067e  4      OPC=movq_r64_m64    
  cqto                            #  15    0x400682  2      OPC=cqto            
  idivq -0x20(%rbp)               #  16    0x400684  4      OPC=idivq_m64       
  movq %rdx, -0x10(%rbp)          #  17    0x400688  4      OPC=movq_m64_r64    
  movq -0x10(%rbp), %rax          #  18    0x40068c  4      OPC=movq_r64_m64    
  cmpq -0x20(%rbp), %rax          #  19    0x400690  4      OPC=cmpq_r64_m64    
  jge .L_4006b6                   #  20    0x400694  2      OPC=jge_label       
  cmpq $0x0, -0x10(%rbp)          #  21    0x400696  5      OPC=cmpq_m64_imm8   
  jne .L_4006a4                   #  22    0x40069b  2      OPC=jne_label       
  nop                             #  23    0x40069d  1      OPC=nop             
  movq -0x20(%rbp), %rax          #  24    0x40069e  4      OPC=movq_r64_m64    
  jmpq .L_4006c1                  #  25    0x4006a2  2      OPC=jmpq_label      
.L_4006a4:                        #        0x4006a4  0      OPC=<label>         
  movq -0x20(%rbp), %rax          #  26    0x4006a4  4      OPC=movq_r64_m64    
  movq %rax, -0x18(%rbp)          #  27    0x4006a8  4      OPC=movq_m64_r64    
  movq -0x10(%rbp), %rax          #  28    0x4006ac  4      OPC=movq_r64_m64    
  movq %rax, -0x20(%rbp)          #  29    0x4006b0  4      OPC=movq_m64_r64    
  jmpq .L_4006bf                  #  30    0x4006b4  2      OPC=jmpq_label      
.L_4006b6:                        #        0x4006b6  0      OPC=<label>         
  movq $0xffffffffffffffff, %rax  #  31    0x4006b6  7      OPC=movq_r64_imm32  
  jmpq .L_4006c1                  #  32    0x4006bd  2      OPC=jmpq_label      
.L_4006bf:                        #        0x4006bf  0      OPC=<label>         
  jmpq .L_40067e                  #  33    0x4006bf  2      OPC=jmpq_label      
.L_4006c1:                        #        0x4006c1  0      OPC=<label>         
  popq %rbp                       #  34    0x4006c1  1      OPC=popq_r64_1      
  retq                            #  35    0x4006c2  1      OPC=retq            
                                                                                
.size gcd_euclid_a, .-gcd_euclid_a

