  .text
  .globl main
  .type main, @function

#! file-offset 0x596
#! rip-offset  0x400596
#! capacity    186 bytes

# Text                      #  Line  RIP       Bytes  Opcode              
.main:                      #        0x400596  0      OPC=<label>         
  pushq %rbp                #  1     0x400596  1      OPC=pushq_r64_1     
  movq %rsp, %rbp           #  2     0x400597  3      OPC=movq_r64_r64    
  pushq %rbx                #  3     0x40059a  1      OPC=pushq_r64_1     
  subq $0x28, %rsp          #  4     0x40059b  4      OPC=subq_r64_imm8   
  movl %edi, -0x24(%rbp)    #  5     0x40059f  3      OPC=movl_m32_r32    
  movq %rsi, -0x30(%rbp)    #  6     0x4005a2  4      OPC=movq_m64_r64    
  cmpl $0x1, -0x24(%rbp)    #  7     0x4005a6  4      OPC=cmpl_m32_imm8   
  jg .L_4005cf              #  8     0x4005aa  2      OPC=jg_label        
  movq -0x30(%rbp), %rax    #  9     0x4005ac  4      OPC=movq_r64_m64    
  movq (%rax), %rax         #  10    0x4005b0  3      OPC=movq_r64_m64    
  movq %rax, %rsi           #  11    0x4005b3  3      OPC=movq_r64_r64    
  movl $0x4007d8, %edi      #  12    0x4005b6  5      OPC=movl_r32_imm32  
  movl $0x0, %eax           #  13    0x4005bb  5      OPC=movl_r32_imm32  
  callq .printf_plt         #  14    0x4005c0  5      OPC=callq_label     
  movl $0x1, %edi           #  15    0x4005c5  5      OPC=movl_r32_imm32  
  callq .exit_plt           #  16    0x4005ca  5      OPC=callq_label     
.L_4005cf:                  #        0x4005cf  0      OPC=<label>         
  movq -0x30(%rbp), %rax    #  17    0x4005cf  4      OPC=movq_r64_m64    
  addq $0x8, %rax           #  18    0x4005d3  4      OPC=addq_r64_imm8   
  movq (%rax), %rax         #  19    0x4005d7  3      OPC=movq_r64_m64    
  movq %rax, %rdi           #  20    0x4005da  3      OPC=movq_r64_r64    
  callq .atol_plt           #  21    0x4005dd  5      OPC=callq_label     
  movq %rax, -0x18(%rbp)    #  22    0x4005e2  4      OPC=movq_m64_r64    
  movq -0x30(%rbp), %rax    #  23    0x4005e6  4      OPC=movq_r64_m64    
  addq $0x10, %rax          #  24    0x4005ea  4      OPC=addq_r64_imm8   
  movq (%rax), %rax         #  25    0x4005ee  3      OPC=movq_r64_m64    
  movq %rax, %rdi           #  26    0x4005f1  3      OPC=movq_r64_r64    
  callq .atol_plt           #  27    0x4005f4  5      OPC=callq_label     
  movq %rax, -0x20(%rbp)    #  28    0x4005f9  4      OPC=movq_m64_r64    
  movq -0x20(%rbp), %rdx    #  29    0x4005fd  4      OPC=movq_r64_m64    
  movq -0x18(%rbp), %rax    #  30    0x400601  4      OPC=movq_r64_m64    
  movq %rdx, %rsi           #  31    0x400605  3      OPC=movq_r64_r64    
  movq %rax, %rdi           #  32    0x400608  3      OPC=movq_r64_r64    
  callq .gcd_euclid_a       #  33    0x40060b  5      OPC=callq_label     
  movq %rax, %rbx           #  34    0x400610  3      OPC=movq_r64_r64    
  movq -0x20(%rbp), %rdx    #  35    0x400613  4      OPC=movq_r64_m64    
  movq -0x18(%rbp), %rax    #  36    0x400617  4      OPC=movq_r64_m64    
  movq %rdx, %rsi           #  37    0x40061b  3      OPC=movq_r64_r64    
  movq %rax, %rdi           #  38    0x40061e  3      OPC=movq_r64_r64    
  callq .gcd_euclid_b       #  39    0x400621  5      OPC=callq_label     
  leaq (%rbx,%rax,1), %rcx  #  40    0x400626  4      OPC=leaq_r64_m16    
  movq -0x20(%rbp), %rdx    #  41    0x40062a  4      OPC=movq_r64_m64    
  movq -0x18(%rbp), %rax    #  42    0x40062e  4      OPC=movq_r64_m64    
  movq %rax, %rsi           #  43    0x400632  3      OPC=movq_r64_r64    
  movl $0x4007f0, %edi      #  44    0x400635  5      OPC=movl_r32_imm32  
  movl $0x0, %eax           #  45    0x40063a  5      OPC=movl_r32_imm32  
  callq .printf_plt         #  46    0x40063f  5      OPC=callq_label     
  movl $0x0, %eax           #  47    0x400644  5      OPC=movl_r32_imm32  
  addq $0x28, %rsp          #  48    0x400649  4      OPC=addq_r64_imm8   
  popq %rbx                 #  49    0x40064d  1      OPC=popq_r64_1      
  popq %rbp                 #  50    0x40064e  1      OPC=popq_r64_1      
  retq                      #  51    0x40064f  1      OPC=retq            
                                                                          
.size main, .-main

