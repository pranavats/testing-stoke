  .text
  .globl _start
  .type _start, @function

#! file-offset 0x4a0
#! rip-offset  0x4004a0
#! capacity    48 bytes

# Text                          #  Line  RIP       Bytes  Opcode              
._start:                        #        0x4004a0  0      OPC=<label>         
  xorl %ebp, %ebp               #  1     0x4004a0  2      OPC=xorl_r32_r32    
  movq %rdx, %r9                #  2     0x4004a2  3      OPC=movq_r64_r64    
  popq %rsi                     #  3     0x4004a5  1      OPC=popq_r64_1      
  movq %rsp, %rdx               #  4     0x4004a6  3      OPC=movq_r64_r64    
  andq $0xfffffff0, %rsp        #  5     0x4004a9  4      OPC=andq_r64_imm8   
  pushq %rax                    #  6     0x4004ad  1      OPC=pushq_r64_1     
  pushq %rsp                    #  7     0x4004ae  1      OPC=pushq_r64_1     
  movq $0x4007c0, %r8           #  8     0x4004af  7      OPC=movq_r64_imm32  
  movq $0x400750, %rcx          #  9     0x4004b6  7      OPC=movq_r64_imm32  
  movq $0x400596, %rdi          #  10    0x4004bd  7      OPC=movq_r64_imm32  
  callq .__libc_start_main_plt  #  11    0x4004c4  5      OPC=callq_label     
  retq                          #  12    0x4004c9  1      OPC=retq            
  nop                           #  13    0x4004ca  1      OPC=nop             
  nop                           #  14    0x4004cb  1      OPC=nop             
  nop                           #  15    0x4004cc  1      OPC=nop             
  nop                           #  16    0x4004cd  1      OPC=nop             
  nop                           #  17    0x4004ce  1      OPC=nop             
  nop                           #  18    0x4004cf  1      OPC=nop             
                                                                              
.size _start, .-_start

