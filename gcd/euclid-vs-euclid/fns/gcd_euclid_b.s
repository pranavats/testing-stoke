  .text
  .globl gcd_euclid_b
  .type gcd_euclid_b, @function

#! file-offset 0x6c3
#! rip-offset  0x4006c3
#! capacity    141 bytes

# Text                            #  Line  RIP       Bytes  Opcode              
.gcd_euclid_b:                    #        0x4006c3  0      OPC=<label>         
  pushq %rbp                      #  1     0x4006c3  1      OPC=pushq_r64_1     
  movq %rsp, %rbp                 #  2     0x4006c4  3      OPC=movq_r64_r64    
  movq %rdi, -0x18(%rbp)          #  3     0x4006c7  4      OPC=movq_m64_r64    
  movq %rsi, -0x20(%rbp)          #  4     0x4006cb  4      OPC=movq_m64_r64    
  movq $0x2d, -0x8(%rbp)          #  5     0x4006cf  8      OPC=movq_m64_imm32  
  movq $0x38, -0x10(%rbp)         #  6     0x4006d7  8      OPC=movq_m64_imm32  
  movq -0x20(%rbp), %rax          #  7     0x4006df  4      OPC=movq_r64_m64    
  cmpq -0x18(%rbp), %rax          #  8     0x4006e3  4      OPC=cmpq_r64_m64    
  jge .L_400701                   #  9     0x4006e7  2      OPC=jge_label       
  movq -0x18(%rbp), %rax          #  10    0x4006e9  4      OPC=movq_r64_m64    
  movq %rax, -0x10(%rbp)          #  11    0x4006ed  4      OPC=movq_m64_r64    
  movq -0x20(%rbp), %rax          #  12    0x4006f1  4      OPC=movq_r64_m64    
  movq %rax, -0x18(%rbp)          #  13    0x4006f5  4      OPC=movq_m64_r64    
  movq -0x10(%rbp), %rax          #  14    0x4006f9  4      OPC=movq_r64_m64    
  movq %rax, -0x20(%rbp)          #  15    0x4006fd  4      OPC=movq_m64_r64    
.L_400701:                        #        0x400701  0      OPC=<label>         
  movq -0x20(%rbp), %rax          #  16    0x400701  4      OPC=movq_r64_m64    
  cqto                            #  17    0x400705  2      OPC=cqto            
  idivq -0x18(%rbp)               #  18    0x400707  4      OPC=idivq_m64       
  movq %rdx, -0x8(%rbp)           #  19    0x40070b  4      OPC=movq_m64_r64    
  movq -0x8(%rbp), %rax           #  20    0x40070f  4      OPC=movq_r64_m64    
  cmpq -0x18(%rbp), %rax          #  21    0x400713  4      OPC=cmpq_r64_m64    
  jge .L_400739                   #  22    0x400717  2      OPC=jge_label       
  cmpq $0x0, -0x8(%rbp)           #  23    0x400719  5      OPC=cmpq_m64_imm8   
  jne .L_400727                   #  24    0x40071e  2      OPC=jne_label       
  nop                             #  25    0x400720  1      OPC=nop             
  movq -0x18(%rbp), %rax          #  26    0x400721  4      OPC=movq_r64_m64    
  jmpq .L_400744                  #  27    0x400725  2      OPC=jmpq_label      
.L_400727:                        #        0x400727  0      OPC=<label>         
  movq -0x18(%rbp), %rax          #  28    0x400727  4      OPC=movq_r64_m64    
  movq %rax, -0x20(%rbp)          #  29    0x40072b  4      OPC=movq_m64_r64    
  movq -0x8(%rbp), %rax           #  30    0x40072f  4      OPC=movq_r64_m64    
  movq %rax, -0x18(%rbp)          #  31    0x400733  4      OPC=movq_m64_r64    
  jmpq .L_400742                  #  32    0x400737  2      OPC=jmpq_label      
.L_400739:                        #        0x400739  0      OPC=<label>         
  movq $0xffffffffffffffff, %rax  #  33    0x400739  7      OPC=movq_r64_imm32  
  jmpq .L_400744                  #  34    0x400740  2      OPC=jmpq_label      
.L_400742:                        #        0x400742  0      OPC=<label>         
  jmpq .L_400701                  #  35    0x400742  2      OPC=jmpq_label      
.L_400744:                        #        0x400744  0      OPC=<label>         
  popq %rbp                       #  36    0x400744  1      OPC=popq_r64_1      
  retq                            #  37    0x400745  1      OPC=retq            
  nop                             #  38    0x400746  1      OPC=nop             
  nop                             #  39    0x400747  1      OPC=nop             
  nop                             #  40    0x400748  1      OPC=nop             
  nop                             #  41    0x400749  1      OPC=nop             
  nop                             #  42    0x40074a  1      OPC=nop             
  nop                             #  43    0x40074b  1      OPC=nop             
  nop                             #  44    0x40074c  1      OPC=nop             
  nop                             #  45    0x40074d  1      OPC=nop             
  nop                             #  46    0x40074e  1      OPC=nop             
  nop                             #  47    0x40074f  1      OPC=nop             
                                                                                
.size gcd_euclid_b, .-gcd_euclid_b

