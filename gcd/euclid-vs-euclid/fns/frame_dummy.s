  .text
  .globl frame_dummy
  .type frame_dummy, @function

#! file-offset 0x570
#! rip-offset  0x400570
#! capacity    38 bytes

# Text                      #  Line  RIP       Bytes  Opcode              
.frame_dummy:               #        0x400570  0      OPC=<label>         
  movl $0x6009b0, %edi      #  1     0x400570  5      OPC=movl_r32_imm32  
  cmpq $0x0, (%rdi)         #  2     0x400575  4      OPC=cmpq_m64_imm8   
  jne .L_400580             #  3     0x400579  2      OPC=jne_label       
.L_40057b:                  #        0x40057b  0      OPC=<label>         
  jmpq .register_tm_clones  #  4     0x40057b  2      OPC=jmpq_label      
  nop                       #  5     0x40057d  1      OPC=nop             
  nop                       #  6     0x40057e  1      OPC=nop             
  nop                       #  7     0x40057f  1      OPC=nop             
.L_400580:                  #        0x400580  0      OPC=<label>         
  movl $0x0, %eax           #  8     0x400580  5      OPC=movl_r32_imm32  
  testq %rax, %rax          #  9     0x400585  3      OPC=testq_r64_r64   
  je .L_40057b              #  10    0x400588  2      OPC=je_label        
  pushq %rbp                #  11    0x40058a  1      OPC=pushq_r64_1     
  movq %rsp, %rbp           #  12    0x40058b  3      OPC=movq_r64_r64    
  callq %rax                #  13    0x40058e  2      OPC=callq_r64       
  popq %rbp                 #  14    0x400590  1      OPC=popq_r64_1      
  jmpq .register_tm_clones  #  15    0x400591  5      OPC=jmpq_label_1    
                                                                          
.size frame_dummy, .-frame_dummy

