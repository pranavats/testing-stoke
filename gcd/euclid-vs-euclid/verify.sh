#!/bin/sh
src=$1".c"
bin=$1".out"
echo $bin

# compile
gcc $src -o ./bin/$bin

# Extract
stoke extract -i ./bin/$bin -o fns-$1

# Testcase

# for Function 1
sed -i "/--fxn/c\--fxn $2" tc.conf
stoke testcase --config tc.conf

sed -i "/--fxn/c\fxn $3" tc.conf
stoke testcase --config tc.conf


# Verify
time stoke debug verify --config verify.conf > verify-$1.conf

