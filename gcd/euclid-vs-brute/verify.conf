##### /home/pranav/backups/Softwares/sources/programming/re/stoke/stoke/bin/stoke_debug_verify

##### Cost Function Evaluation Options:

# Give up once cost exceeds this value
# --max_cost <int>
# Default: 4611686018427387903

# The cost function.  Can be an arbitrary expression involving the following constructs:
# - arithmetic operators: + - * / % == << >> < > >= <= & |
# - binsize: Size of the binary
# - correctness: Correctness according to the testcases
# - latency: Latency of the instructions
# - measured: Measured latency (more precise for loops than 'latency')
# - size: The number of instructions
# - sseavx: 1 if both sse and avx instructions are used, 0 otherwise
# - nongoal: 1 if the code is exactly the same as one provided via --non_goal
# --cost <string>
# Default: "correctness+measured"

# Expression to check if code is correct
# --correctness <string>
# Default: "correctness == 0"

##### Correctness Options:

# Metric for measuring distance between states
# --distance (hamming|ulp|extension)
# Default: hamming

# Reduction method
# --reduction (max|sum|extension)
# Default: sum

# Number of bytes in sse elements
# --sse_width (1|2|4|8)
# Default: 8

# Number of values in sse registers
# --sse_count <int>
# Default: 4

# Disallow correct values in incorrect register locations
# --no_relax_reg 

# Allow correct values in incorrect memory locations
 --relax_mem 

# Enables an optimized version of relax_mem that assumes heap writes occur in 128-bit blocks
 --blocked_heap_opt 

# Penalty for correct values in incorrect locations
# --misalign_penalty <int>
# Default: 1

# Penalty for incorrect signal behavior
# --sig_penalty <int>
# Default: 10000

# Minimum ULP value to record
# --min_ulp <int>
# Default: 0

##### Input/Output Register/Memory Options:

# Registers defined on entry
# --def_in { %rax %rsp ... }
# Default: { %rax %rcx %rdx %rsi %rdi %r8 %r9 %xmm0 %xmm1 %xmm2 %xmm3 %xmm4 %xmm5 %xmm6 %xmm7 }

# Registers live on exit
# --live_out { %rax %rsp ... }
# Default: { %rax %rdx %xmm0 %xmm1 }

# Is stack defined on exit?
# --stack_out 

# Is heap defined on exit?
# --heap_out 

# Remove the default rounding control bit (%mxcsr[rc]) from def_in
# --no_default_mxcsr 

# Skip sanity checks on def-ins, live-outs, and instruction support.  Useful for verification.
# --live_dangerously 

##### "latency" Cost Function Options:

# Latency multiplier for nested code
# --nesting_penalty <int>
# Default: 5

##### "nongoal" Cost Function Options:

# Directory containing assembly codes that score a value of 1 in the cost-function component 'nongoal'.  Can be used to steer the search towards other codes.
# --non_goal <path/to/dir>

##### Auxiliary Function Options:

# Directory containing helper functions
# --functions <path/to/dir>

# Automatically remove target and unreachable functions from functions directory
# --prune 

##### Target Options:

# Target code
 --target ./fns/gcd_brute.s

##### Rewrite Options:

# Rewrite code
 --rewrite ./fns/gcd_euclid.s

##### Sandbox Options:

# Report SIGSEGV for abi violations
# --abi_check 

# Report SIGSEGV for stack smashing violations
# --stack_check 

# Maximum jumps before exit due to infinite loop
# --max_jumps <int>
# Default: 1024

##### Random Seed Options:

# Random seed for stoke tools; set to zero for random
# --seed <int>
# Default: 0

##### Formal Validator Options:

# SMT Solver backend
# --solver (cvc4|z3)
# Default: z3

# Timeout in milliseconds for SMT solver before giving up.  0 for no limit.
# --solver_timeout <int>
# Default: 0

##### Testcase Options:

# Testcases
 --testcases testcase-results.tc

# Shuffle testcase ordering
# --shuffle_testcases 

# Subset of testcase indices to use for training sets
# --training_set { 0 1 ... 9 }
# Default: { 0 ... 3 }

# Subset of testcase indices to use for test sets
# --test_set { 0 1 ... 9 }
# Default: { 0 ... 9999 }

# Subset of testcase indices to use for performance sets
# --performance_set { 0 1 ... 9 }
# Default: { 0 ... 3 }

# Testcase index
# --index <int>
# Default: 0

##### Bounded Verifier Options:

# Maximum iterations through a loop
# --bound <int>
# Default: 2

# Do not bailout once first counterexample found
# --no_early_bailout 

##### DDEC Verifier Options:

# Do not try to find invariants using sign extension
# --no_try_sign_extend 

# Do not use the bounded validator to refine DDEC invariants.
# --no_ddec_bv 

# Use sound nullspace computation over bitvectors.
# --sound_nullspace 

##### Verification Options:

# Verification strategy
 --strategy ddec #(none|bounded|ddec|hold_out)
# Default: "hold_out"

##### Common Formal Validation Options:

# How to handle aliasing
# --alias_strategy (basic|string|string_antialias|flat)
# Default: "flat"

# add constraints to bound index registers away from 32-bit boundary
# --verify_nacl 

##### Diff Options:

# Show unchanged lines
 --show_unchanged 

# Show changes in all registers, not just the ones from live_out and def_in
 --diff_all_registers 

# Machine-readable output (result and counterexample)
# --machine_output <path/to/file.s>

##### Help and argument utilities:

# Print this message and quit
# --help 

# Print program arguments and quit
# --debug_args 

# Read program args from a configuration file
# --config <path/to/file.conf>

# Print an example configuration file
# --example_config <path/to/file.conf>

