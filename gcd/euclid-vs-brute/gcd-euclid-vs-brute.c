#include <stdio.h>
#include <stdlib.h>

#define NOP __asm__("nop");
//#pragma GCC push_options
//#pragma GCC OPTIMIZE ("unroll-loops")
long gcd_euclid(long, long);
long gcd_brute(long, long);
int main(const int argc, const char* argv[]) {

    if(argc < 2) {
        printf("\n\tUsage: %s <number>\n", argv[0]);
        exit(1);
    } else {
        long a, b;
        a = atol(argv[1]);
        b = a + rand(); /* atol(argv[2]); */
        printf("\n\tDouble of gcd of %ld & %ld is %ld.\n",
               a, b, gcd_euclid(a, b) + gcd_brute(a, b));
    }
    return 0;
}

/* Test for equivalence check */
//__attribute__((optimize("unroll-loops")))
long gcd_euclid(long a, long b) {

    long r = 0, tmp = 0;

    //GCD using Euclid's Algorithm
    if(a < b) {
        tmp = a;
        a = b;
        b = tmp;
    }

    while(1) {
        r = a % b;
        if(r < b) {
            if(r == 0)
                break; //b is gcd
            a = b;
            b = r;
        } else
            return -1;
    }

    return b;
}

long gcd_brute(long a, long b) {
    long i;
    for(i = a; i >= 0; i--) {
        if(a%i == 0 && b%i == 0) {
            break;
        }
    }
    return i;
}
