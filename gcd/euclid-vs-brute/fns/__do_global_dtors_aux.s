  .text
  .globl __do_global_dtors_aux
  .type __do_global_dtors_aux, @function

#! file-offset 0x590
#! rip-offset  0x400590
#! capacity    32 bytes

# Text                         #  Line  RIP       Bytes  Opcode            
.__do_global_dtors_aux:        #        0x400590  0      OPC=<label>       
  cmpb $0x0, 0x200661(%rip)    #  1     0x400590  7      OPC=cmpb_m8_imm8  
  jne .L_4005aa                #  2     0x400597  2      OPC=jne_label     
  pushq %rbp                   #  3     0x400599  1      OPC=pushq_r64_1   
  movq %rsp, %rbp              #  4     0x40059a  3      OPC=movq_r64_r64  
  callq .deregister_tm_clones  #  5     0x40059d  5      OPC=callq_label   
  popq %rbp                    #  6     0x4005a2  1      OPC=popq_r64_1    
  movb $0x1, 0x20064e(%rip)    #  7     0x4005a3  7      OPC=movb_m8_imm8  
.L_4005aa:                     #        0x4005aa  0      OPC=<label>       
  nop                          #  8     0x4005aa  1      OPC=nop           
  retq                         #  9     0x4005ab  1      OPC=retq          
  nop                          #  10    0x4005ac  1      OPC=nop           
  nop                          #  11    0x4005ad  1      OPC=nop           
  nop                          #  12    0x4005ae  1      OPC=nop           
  nop                          #  13    0x4005af  1      OPC=nop           
                                                                           
.size __do_global_dtors_aux, .-__do_global_dtors_aux

