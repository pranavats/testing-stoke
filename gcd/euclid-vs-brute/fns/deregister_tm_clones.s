  .text
  .globl deregister_tm_clones
  .type deregister_tm_clones, @function

#! file-offset 0x510
#! rip-offset  0x400510
#! capacity    64 bytes

# Text                  #  Line  RIP       Bytes  Opcode              
.deregister_tm_clones:  #        0x400510  0      OPC=<label>         
  movl $0x600bff, %eax  #  1     0x400510  5      OPC=movl_r32_imm32  
  pushq %rbp            #  2     0x400515  1      OPC=pushq_r64_1     
  subq $0x600bf8, %rax  #  3     0x400516  6      OPC=subq_rax_imm32  
  cmpq $0xe, %rax       #  4     0x40051c  4      OPC=cmpq_r64_imm8   
  movq %rsp, %rbp       #  5     0x400520  3      OPC=movq_r64_r64    
  jbe .L_400540         #  6     0x400523  2      OPC=jbe_label       
  movl $0x0, %eax       #  7     0x400525  5      OPC=movl_r32_imm32  
  testq %rax, %rax      #  8     0x40052a  3      OPC=testq_r64_r64   
  je .L_400540          #  9     0x40052d  2      OPC=je_label        
  popq %rbp             #  10    0x40052f  1      OPC=popq_r64_1      
  movl $0x600bf8, %edi  #  11    0x400530  5      OPC=movl_r32_imm32  
  jmpq %rax             #  12    0x400535  2      OPC=jmpq_r64        
  nop                   #  13    0x400537  1      OPC=nop             
  nop                   #  14    0x400538  1      OPC=nop             
  nop                   #  15    0x400539  1      OPC=nop             
  nop                   #  16    0x40053a  1      OPC=nop             
  nop                   #  17    0x40053b  1      OPC=nop             
  nop                   #  18    0x40053c  1      OPC=nop             
  nop                   #  19    0x40053d  1      OPC=nop             
  nop                   #  20    0x40053e  1      OPC=nop             
  nop                   #  21    0x40053f  1      OPC=nop             
.L_400540:              #        0x400540  0      OPC=<label>         
  popq %rbp             #  22    0x400540  1      OPC=popq_r64_1      
  retq                  #  23    0x400541  1      OPC=retq            
  nop                   #  24    0x400542  1      OPC=nop             
  nop                   #  25    0x400543  1      OPC=nop             
  nop                   #  26    0x400544  1      OPC=nop             
  nop                   #  27    0x400545  1      OPC=nop             
  nop                   #  28    0x400546  1      OPC=nop             
  nop                   #  29    0x400547  1      OPC=nop             
  nop                   #  30    0x400548  1      OPC=nop             
  nop                   #  31    0x400549  1      OPC=nop             
  nop                   #  32    0x40054a  1      OPC=nop             
  nop                   #  33    0x40054b  1      OPC=nop             
  nop                   #  34    0x40054c  1      OPC=nop             
  nop                   #  35    0x40054d  1      OPC=nop             
  nop                   #  36    0x40054e  1      OPC=nop             
  nop                   #  37    0x40054f  1      OPC=nop             
                                                                      
.size deregister_tm_clones, .-deregister_tm_clones

