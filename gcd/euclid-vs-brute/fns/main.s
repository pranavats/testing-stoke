  .text
  .globl main
  .type main, @function

#! file-offset 0x5d6
#! rip-offset  0x4005d6
#! capacity    182 bytes

# Text                      #  Line  RIP       Bytes  Opcode              
.main:                      #        0x4005d6  0      OPC=<label>         
  pushq %rbp                #  1     0x4005d6  1      OPC=pushq_r64_1     
  movq %rsp, %rbp           #  2     0x4005d7  3      OPC=movq_r64_r64    
  pushq %rbx                #  3     0x4005da  1      OPC=pushq_r64_1     
  subq $0x28, %rsp          #  4     0x4005db  4      OPC=subq_r64_imm8   
  movl %edi, -0x24(%rbp)    #  5     0x4005df  3      OPC=movl_m32_r32    
  movq %rsi, -0x30(%rbp)    #  6     0x4005e2  4      OPC=movq_m64_r64    
  cmpl $0x1, -0x24(%rbp)    #  7     0x4005e6  4      OPC=cmpl_m32_imm8   
  jg .L_40060f              #  8     0x4005ea  2      OPC=jg_label        
  movq -0x30(%rbp), %rax    #  9     0x4005ec  4      OPC=movq_r64_m64    
  movq (%rax), %rax         #  10    0x4005f0  3      OPC=movq_r64_m64    
  movq %rax, %rsi           #  11    0x4005f3  3      OPC=movq_r64_r64    
  movl $0x4007e8, %edi      #  12    0x4005f6  5      OPC=movl_r32_imm32  
  movl $0x0, %eax           #  13    0x4005fb  5      OPC=movl_r32_imm32  
  callq .printf_plt         #  14    0x400600  5      OPC=callq_label     
  movl $0x1, %edi           #  15    0x400605  5      OPC=movl_r32_imm32  
  callq .exit_plt           #  16    0x40060a  5      OPC=callq_label     
.L_40060f:                  #        0x40060f  0      OPC=<label>         
  movq -0x30(%rbp), %rax    #  17    0x40060f  4      OPC=movq_r64_m64    
  addq $0x8, %rax           #  18    0x400613  4      OPC=addq_r64_imm8   
  movq (%rax), %rax         #  19    0x400617  3      OPC=movq_r64_m64    
  movq %rax, %rdi           #  20    0x40061a  3      OPC=movq_r64_r64    
  callq .atol_plt           #  21    0x40061d  5      OPC=callq_label     
  movq %rax, -0x18(%rbp)    #  22    0x400622  4      OPC=movq_m64_r64    
  callq .rand_plt           #  23    0x400626  5      OPC=callq_label     
  movslq %eax, %rdx         #  24    0x40062b  3      OPC=movslq_r64_r32  
  movq -0x18(%rbp), %rax    #  25    0x40062e  4      OPC=movq_r64_m64    
  addq %rdx, %rax           #  26    0x400632  3      OPC=addq_r64_r64    
  movq %rax, -0x20(%rbp)    #  27    0x400635  4      OPC=movq_m64_r64    
  movq -0x20(%rbp), %rdx    #  28    0x400639  4      OPC=movq_r64_m64    
  movq -0x18(%rbp), %rax    #  29    0x40063d  4      OPC=movq_r64_m64    
  movq %rdx, %rsi           #  30    0x400641  3      OPC=movq_r64_r64    
  movq %rax, %rdi           #  31    0x400644  3      OPC=movq_r64_r64    
  callq .gcd_euclid         #  32    0x400647  5      OPC=callq_label     
  movq %rax, %rbx           #  33    0x40064c  3      OPC=movq_r64_r64    
  movq -0x20(%rbp), %rdx    #  34    0x40064f  4      OPC=movq_r64_m64    
  movq -0x18(%rbp), %rax    #  35    0x400653  4      OPC=movq_r64_m64    
  movq %rdx, %rsi           #  36    0x400657  3      OPC=movq_r64_r64    
  movq %rax, %rdi           #  37    0x40065a  3      OPC=movq_r64_r64    
  callq .gcd_brute          #  38    0x40065d  5      OPC=callq_label     
  leaq (%rbx,%rax,1), %rcx  #  39    0x400662  4      OPC=leaq_r64_m16    
  movq -0x20(%rbp), %rdx    #  40    0x400666  4      OPC=movq_r64_m64    
  movq -0x18(%rbp), %rax    #  41    0x40066a  4      OPC=movq_r64_m64    
  movq %rax, %rsi           #  42    0x40066e  3      OPC=movq_r64_r64    
  movl $0x400800, %edi      #  43    0x400671  5      OPC=movl_r32_imm32  
  movl $0x0, %eax           #  44    0x400676  5      OPC=movl_r32_imm32  
  callq .printf_plt         #  45    0x40067b  5      OPC=callq_label     
  movl $0x0, %eax           #  46    0x400680  5      OPC=movl_r32_imm32  
  addq $0x28, %rsp          #  47    0x400685  4      OPC=addq_r64_imm8   
  popq %rbx                 #  48    0x400689  1      OPC=popq_r64_1      
  popq %rbp                 #  49    0x40068a  1      OPC=popq_r64_1      
  retq                      #  50    0x40068b  1      OPC=retq            
                                                                          
.size main, .-main

