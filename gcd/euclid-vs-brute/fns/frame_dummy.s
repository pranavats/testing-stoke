  .text
  .globl frame_dummy
  .type frame_dummy, @function

#! file-offset 0x5b0
#! rip-offset  0x4005b0
#! capacity    38 bytes

# Text                      #  Line  RIP       Bytes  Opcode              
.frame_dummy:               #        0x4005b0  0      OPC=<label>         
  movl $0x6009c0, %edi      #  1     0x4005b0  5      OPC=movl_r32_imm32  
  cmpq $0x0, (%rdi)         #  2     0x4005b5  4      OPC=cmpq_m64_imm8   
  jne .L_4005c0             #  3     0x4005b9  2      OPC=jne_label       
.L_4005bb:                  #        0x4005bb  0      OPC=<label>         
  jmpq .register_tm_clones  #  4     0x4005bb  2      OPC=jmpq_label      
  nop                       #  5     0x4005bd  1      OPC=nop             
  nop                       #  6     0x4005be  1      OPC=nop             
  nop                       #  7     0x4005bf  1      OPC=nop             
.L_4005c0:                  #        0x4005c0  0      OPC=<label>         
  movl $0x0, %eax           #  8     0x4005c0  5      OPC=movl_r32_imm32  
  testq %rax, %rax          #  9     0x4005c5  3      OPC=testq_r64_r64   
  je .L_4005bb              #  10    0x4005c8  2      OPC=je_label        
  pushq %rbp                #  11    0x4005ca  1      OPC=pushq_r64_1     
  movq %rsp, %rbp           #  12    0x4005cb  3      OPC=movq_r64_r64    
  callq %rax                #  13    0x4005ce  2      OPC=callq_r64       
  popq %rbp                 #  14    0x4005d0  1      OPC=popq_r64_1      
  jmpq .register_tm_clones  #  15    0x4005d1  5      OPC=jmpq_label_1    
                                                                          
.size frame_dummy, .-frame_dummy

