  .text
  .globl gcd_brute
  .type gcd_brute, @function

#! file-offset 0x70f
#! rip-offset  0x40070f
#! capacity    81 bytes

# Text                    #  Line  RIP       Bytes  Opcode             
.gcd_brute:               #        0x40070f  0      OPC=<label>        
  pushq %rbp              #  1     0x40070f  1      OPC=pushq_r64_1    
  movq %rsp, %rbp         #  2     0x400710  3      OPC=movq_r64_r64   
  movq %rdi, -0x18(%rbp)  #  3     0x400713  4      OPC=movq_m64_r64   
  movq %rsi, -0x20(%rbp)  #  4     0x400717  4      OPC=movq_m64_r64   
  movq -0x18(%rbp), %rax  #  5     0x40071b  4      OPC=movq_r64_m64   
  movq %rax, -0x8(%rbp)   #  6     0x40071f  4      OPC=movq_m64_r64   
  jmpq .L_400750          #  7     0x400723  2      OPC=jmpq_label     
.L_400725:                #        0x400725  0      OPC=<label>        
  movq -0x18(%rbp), %rax  #  8     0x400725  4      OPC=movq_r64_m64   
  cqto                    #  9     0x400729  2      OPC=cqto           
  idivq -0x8(%rbp)        #  10    0x40072b  4      OPC=idivq_m64      
  movq %rdx, %rax         #  11    0x40072f  3      OPC=movq_r64_r64   
  testq %rax, %rax        #  12    0x400732  3      OPC=testq_r64_r64  
  jne .L_40074b           #  13    0x400735  2      OPC=jne_label      
  movq -0x20(%rbp), %rax  #  14    0x400737  4      OPC=movq_r64_m64   
  cqto                    #  15    0x40073b  2      OPC=cqto           
  idivq -0x8(%rbp)        #  16    0x40073d  4      OPC=idivq_m64      
  movq %rdx, %rax         #  17    0x400741  3      OPC=movq_r64_r64   
  testq %rax, %rax        #  18    0x400744  3      OPC=testq_r64_r64  
  jne .L_40074b           #  19    0x400747  2      OPC=jne_label      
  jmpq .L_400757          #  20    0x400749  2      OPC=jmpq_label     
.L_40074b:                #        0x40074b  0      OPC=<label>        
  subq $0x1, -0x8(%rbp)   #  21    0x40074b  5      OPC=subq_m64_imm8  
.L_400750:                #        0x400750  0      OPC=<label>        
  cmpq $0x0, -0x8(%rbp)   #  22    0x400750  5      OPC=cmpq_m64_imm8  
  jns .L_400725           #  23    0x400755  2      OPC=jns_label      
.L_400757:                #        0x400757  0      OPC=<label>        
  movq -0x8(%rbp), %rax   #  24    0x400757  4      OPC=movq_r64_m64   
  popq %rbp               #  25    0x40075b  1      OPC=popq_r64_1     
  retq                    #  26    0x40075c  1      OPC=retq           
  nop                     #  27    0x40075d  1      OPC=nop            
  nop                     #  28    0x40075e  1      OPC=nop            
  nop                     #  29    0x40075f  1      OPC=nop            
                                                                       
.size gcd_brute, .-gcd_brute

