  .text
  .globl gcd_euclid
  .type gcd_euclid, @function

#! file-offset 0x68c
#! rip-offset  0x40068c
#! capacity    131 bytes

# Text                            #  Line  RIP       Bytes  Opcode              
.gcd_euclid:                      #        0x40068c  0      OPC=<label>         
  pushq %rbp                      #  1     0x40068c  1      OPC=pushq_r64_1     
  movq %rsp, %rbp                 #  2     0x40068d  3      OPC=movq_r64_r64    
  movq %rdi, -0x18(%rbp)          #  3     0x400690  4      OPC=movq_m64_r64    
  movq %rsi, -0x20(%rbp)          #  4     0x400694  4      OPC=movq_m64_r64    
  movq $0x0, -0x8(%rbp)           #  5     0x400698  8      OPC=movq_m64_imm32  
  movq $0x0, -0x10(%rbp)          #  6     0x4006a0  8      OPC=movq_m64_imm32  
  movq -0x18(%rbp), %rax          #  7     0x4006a8  4      OPC=movq_r64_m64    
  cmpq -0x20(%rbp), %rax          #  8     0x4006ac  4      OPC=cmpq_r64_m64    
  jge .L_4006ca                   #  9     0x4006b0  2      OPC=jge_label       
  movq -0x18(%rbp), %rax          #  10    0x4006b2  4      OPC=movq_r64_m64    
  movq %rax, -0x10(%rbp)          #  11    0x4006b6  4      OPC=movq_m64_r64    
  movq -0x20(%rbp), %rax          #  12    0x4006ba  4      OPC=movq_r64_m64    
  movq %rax, -0x18(%rbp)          #  13    0x4006be  4      OPC=movq_m64_r64    
  movq -0x10(%rbp), %rax          #  14    0x4006c2  4      OPC=movq_r64_m64    
  movq %rax, -0x20(%rbp)          #  15    0x4006c6  4      OPC=movq_m64_r64    
.L_4006ca:                        #        0x4006ca  0      OPC=<label>         
  movq -0x18(%rbp), %rax          #  16    0x4006ca  4      OPC=movq_r64_m64    
  cqto                            #  17    0x4006ce  2      OPC=cqto            
  idivq -0x20(%rbp)               #  18    0x4006d0  4      OPC=idivq_m64       
  movq %rdx, -0x8(%rbp)           #  19    0x4006d4  4      OPC=movq_m64_r64    
  movq -0x8(%rbp), %rax           #  20    0x4006d8  4      OPC=movq_r64_m64    
  cmpq -0x20(%rbp), %rax          #  21    0x4006dc  4      OPC=cmpq_r64_m64    
  jge .L_400702                   #  22    0x4006e0  2      OPC=jge_label       
  cmpq $0x0, -0x8(%rbp)           #  23    0x4006e2  5      OPC=cmpq_m64_imm8   
  jne .L_4006f0                   #  24    0x4006e7  2      OPC=jne_label       
  nop                             #  25    0x4006e9  1      OPC=nop             
  movq -0x20(%rbp), %rax          #  26    0x4006ea  4      OPC=movq_r64_m64    
  jmpq .L_40070d                  #  27    0x4006ee  2      OPC=jmpq_label      
.L_4006f0:                        #        0x4006f0  0      OPC=<label>         
  movq -0x20(%rbp), %rax          #  28    0x4006f0  4      OPC=movq_r64_m64    
  movq %rax, -0x18(%rbp)          #  29    0x4006f4  4      OPC=movq_m64_r64    
  movq -0x8(%rbp), %rax           #  30    0x4006f8  4      OPC=movq_r64_m64    
  movq %rax, -0x20(%rbp)          #  31    0x4006fc  4      OPC=movq_m64_r64    
  jmpq .L_40070b                  #  32    0x400700  2      OPC=jmpq_label      
.L_400702:                        #        0x400702  0      OPC=<label>         
  movq $0xffffffffffffffff, %rax  #  33    0x400702  7      OPC=movq_r64_imm32  
  jmpq .L_40070d                  #  34    0x400709  2      OPC=jmpq_label      
.L_40070b:                        #        0x40070b  0      OPC=<label>         
  jmpq .L_4006ca                  #  35    0x40070b  2      OPC=jmpq_label      
.L_40070d:                        #        0x40070d  0      OPC=<label>         
  popq %rbp                       #  36    0x40070d  1      OPC=popq_r64_1      
  retq                            #  37    0x40070e  1      OPC=retq            
                                                                                
.size gcd_euclid, .-gcd_euclid

