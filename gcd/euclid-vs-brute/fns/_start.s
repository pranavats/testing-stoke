  .text
  .globl _start
  .type _start, @function

#! file-offset 0x4e0
#! rip-offset  0x4004e0
#! capacity    48 bytes

# Text                          #  Line  RIP       Bytes  Opcode              
._start:                        #        0x4004e0  0      OPC=<label>         
  xorl %ebp, %ebp               #  1     0x4004e0  2      OPC=xorl_r32_r32    
  movq %rdx, %r9                #  2     0x4004e2  3      OPC=movq_r64_r64    
  popq %rsi                     #  3     0x4004e5  1      OPC=popq_r64_1      
  movq %rsp, %rdx               #  4     0x4004e6  3      OPC=movq_r64_r64    
  andq $0xfffffff0, %rsp        #  5     0x4004e9  4      OPC=andq_r64_imm8   
  pushq %rax                    #  6     0x4004ed  1      OPC=pushq_r64_1     
  pushq %rsp                    #  7     0x4004ee  1      OPC=pushq_r64_1     
  movq $0x4007d0, %r8           #  8     0x4004ef  7      OPC=movq_r64_imm32  
  movq $0x400760, %rcx          #  9     0x4004f6  7      OPC=movq_r64_imm32  
  movq $0x4005d6, %rdi          #  10    0x4004fd  7      OPC=movq_r64_imm32  
  callq .__libc_start_main_plt  #  11    0x400504  5      OPC=callq_label     
  retq                          #  12    0x400509  1      OPC=retq            
  nop                           #  13    0x40050a  1      OPC=nop             
  nop                           #  14    0x40050b  1      OPC=nop             
  nop                           #  15    0x40050c  1      OPC=nop             
  nop                           #  16    0x40050d  1      OPC=nop             
  nop                           #  17    0x40050e  1      OPC=nop             
  nop                           #  18    0x40050f  1      OPC=nop             
                                                                              
.size _start, .-_start

