  .text
  .globl __libc_csu_init
  .type __libc_csu_init, @function

#! file-offset 0x760
#! rip-offset  0x400760
#! capacity    112 bytes

# Text                       #  Line  RIP       Bytes  Opcode             
.__libc_csu_init:            #        0x400760  0      OPC=<label>        
  pushq %r15                 #  1     0x400760  2      OPC=pushq_r64_1    
  movl %edi, %r15d           #  2     0x400762  3      OPC=movl_r32_r32   
  pushq %r14                 #  3     0x400765  2      OPC=pushq_r64_1    
  movq %rsi, %r14            #  4     0x400767  3      OPC=movq_r64_r64   
  pushq %r13                 #  5     0x40076a  2      OPC=pushq_r64_1    
  movq %rdx, %r13            #  6     0x40076c  3      OPC=movq_r64_r64   
  pushq %r12                 #  7     0x40076f  2      OPC=pushq_r64_1    
  leaq 0x200238(%rip), %r12  #  8     0x400771  7      OPC=leaq_r64_m16   
  pushq %rbp                 #  9     0x400778  1      OPC=pushq_r64_1    
  leaq 0x200238(%rip), %rbp  #  10    0x400779  7      OPC=leaq_r64_m16   
  pushq %rbx                 #  11    0x400780  1      OPC=pushq_r64_1    
  subq %r12, %rbp            #  12    0x400781  3      OPC=subq_r64_r64   
  xorl %ebx, %ebx            #  13    0x400784  2      OPC=xorl_r32_r32   
  sarq $0x3, %rbp            #  14    0x400786  4      OPC=sarq_r64_imm8  
  subq $0x8, %rsp            #  15    0x40078a  4      OPC=subq_r64_imm8  
  callq ._init               #  16    0x40078e  5      OPC=callq_label    
  testq %rbp, %rbp           #  17    0x400793  3      OPC=testq_r64_r64  
  je .L_4007b6               #  18    0x400796  2      OPC=je_label       
  nop                        #  19    0x400798  1      OPC=nop            
  nop                        #  20    0x400799  1      OPC=nop            
  nop                        #  21    0x40079a  1      OPC=nop            
  nop                        #  22    0x40079b  1      OPC=nop            
  nop                        #  23    0x40079c  1      OPC=nop            
  nop                        #  24    0x40079d  1      OPC=nop            
  nop                        #  25    0x40079e  1      OPC=nop            
  nop                        #  26    0x40079f  1      OPC=nop            
.L_4007a0:                   #        0x4007a0  0      OPC=<label>        
  movq %r13, %rdx            #  27    0x4007a0  3      OPC=movq_r64_r64   
  movq %r14, %rsi            #  28    0x4007a3  3      OPC=movq_r64_r64   
  movl %r15d, %edi           #  29    0x4007a6  3      OPC=movl_r32_r32   
  callq (%r12,%rbx,8)        #  30    0x4007a9  4      OPC=callq_m64      
  addq $0x1, %rbx            #  31    0x4007ad  4      OPC=addq_r64_imm8  
  cmpq %rbp, %rbx            #  32    0x4007b1  3      OPC=cmpq_r64_r64   
  jne .L_4007a0              #  33    0x4007b4  2      OPC=jne_label      
.L_4007b6:                   #        0x4007b6  0      OPC=<label>        
  addq $0x8, %rsp            #  34    0x4007b6  4      OPC=addq_r64_imm8  
  popq %rbx                  #  35    0x4007ba  1      OPC=popq_r64_1     
  popq %rbp                  #  36    0x4007bb  1      OPC=popq_r64_1     
  popq %r12                  #  37    0x4007bc  2      OPC=popq_r64_1     
  popq %r13                  #  38    0x4007be  2      OPC=popq_r64_1     
  popq %r14                  #  39    0x4007c0  2      OPC=popq_r64_1     
  popq %r15                  #  40    0x4007c2  2      OPC=popq_r64_1     
  retq                       #  41    0x4007c4  1      OPC=retq           
  nop                        #  42    0x4007c5  1      OPC=nop            
  nop                        #  43    0x4007c6  1      OPC=nop            
  nop                        #  44    0x4007c7  1      OPC=nop            
  nop                        #  45    0x4007c8  1      OPC=nop            
  nop                        #  46    0x4007c9  1      OPC=nop            
  nop                        #  47    0x4007ca  1      OPC=nop            
  nop                        #  48    0x4007cb  1      OPC=nop            
  nop                        #  49    0x4007cc  1      OPC=nop            
  nop                        #  50    0x4007cd  1      OPC=nop            
  nop                        #  51    0x4007ce  1      OPC=nop            
  nop                        #  52    0x4007cf  1      OPC=nop            
                                                                          
.size __libc_csu_init, .-__libc_csu_init

